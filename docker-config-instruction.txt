#run maven Docker build
clean install dockerfile:build -Dmaven.test.skip=true

#run postgre sql docker image in detached mode
docker run --rm   --name pg-docker -e POSTGRES_PASSWORD=Passw0rd -d -p 5433:5432 -v $HOME/DEV/docker/volumes/postgres:/var/lib/postgresql/data  postgres

docker network create -d overlay --attachable matchnet

docker stack deploy -c docker-compose.yml matchbackend

docker run --rm -u root -p 8080:8080 -v $HOME/DEV/docker/volumes/jenkins:/var/jenkins_home  -v /var/run/docker.sock:/var/run/docker.sock -v "$HOME":/home jenkinsci/blueocean

docker run -u root -p 8080:8080 -v $HOME/DEV/docker/volumes/jenkins:/var/jenkins_home jenkinsci/blueocean