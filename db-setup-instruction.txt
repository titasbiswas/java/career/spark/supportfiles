

# DB DDL - after creating a user as 'spark' with password as 's3cret'
CREATE DATABASE spark
    WITH 
    OWNER = spark
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE SCHEMA schema_spark
    AUTHORIZATION spark;

CREATE SEQUENCE schema_spark.matches_id_seq;
	ALTER SEQUENCE schema_spark.matches_id_seq
    OWNER TO spark;
    
    
    CREATE TABLE schema_spark.matches
	(
    id bigint NOT NULL DEFAULT nextval('schema_spark.matches_id_seq'::regclass),
    display_name character varying(255) COLLATE pg_catalog."default",
    age integer,
    job_title character varying(255) COLLATE pg_catalog."default",
    height_in_cm integer,
    city_name character varying(100) COLLATE pg_catalog."default",
    lat numeric,
    lon numeric,
    main_photo character varying(255) COLLATE pg_catalog."default",
    compatibility_score numeric,
    contacts_exchanged integer,
    favourite boolean,
    religion character varying(100) COLLATE pg_catalog."default",
	hidden boolean,
    CONSTRAINT matches_pkey PRIMARY KEY (id)
	)
	WITH (
		OIDS = FALSE
	)
	TABLESPACE pg_default;

	ALTER TABLE schema_spark.matches
		OWNER to spark;



# create a database spark_test and replicate all the above steps (create the schema, sequence and table)

CREATE DATABASE spark_test
    WITH 
    OWNER = spark
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;